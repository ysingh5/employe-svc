package com.altimetrik.employe.svc.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.altimetrik.employe.svc.dto.EmployeeDTO;
import com.altimetrik.employe.svc.service.EmployeeService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("api/empsvc")
@AllArgsConstructor
@Slf4j
public class EmployeeController {

	private EmployeeService employeeService;
	
	@PostMapping
	public EmployeeDTO doSave(@RequestBody EmployeeDTO dto){
		log.info("Req: Employee:{}", dto.getName());
		return employeeService.doSave(dto);
	}
	
	
	
	@GetMapping
	public List<EmployeeDTO> getAll(){
		log.info("Req: Employee");
		return employeeService.getAll();
	}
	
	@GetMapping("{id}")
	public EmployeeDTO getById(@PathVariable Long id){
		log.info("Req: Employee by id:{}", id);
		return employeeService.getById(id);
	}
	
	
}
