package com.altimetrik.employe.svc.utills;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.altimetrik.employe.svc.dto.EmployeeDTO;
import com.altimetrik.employe.svc.entity.Employee;

@Mapper
public interface ObjectMapper {
  
	ObjectMapper MAPPER = Mappers.getMapper(ObjectMapper.class);
	//@Mapping()
	EmployeeDTO mapToDepartmentDTO(Employee emp);
	
	Employee mapToDepartment(EmployeeDTO dto);
}
