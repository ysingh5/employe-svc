package com.altimetrik.employe.svc.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class EmployeeDTO {
	private Long id;
	private String name;
	private String email;
	private String departmentName;
	private String departmentCode;
}
