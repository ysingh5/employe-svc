package com.altimetrik.employe.svc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class EmployeSvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeSvcApplication.class, args);
	}

}
