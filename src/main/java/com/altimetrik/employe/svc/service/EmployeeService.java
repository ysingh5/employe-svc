package com.altimetrik.employe.svc.service;

import java.util.List;

import com.altimetrik.employe.svc.dto.EmployeeDTO;

public interface EmployeeService  {
   EmployeeDTO doSave(EmployeeDTO dto);
   
   EmployeeDTO getById(Long id);
   
   List<EmployeeDTO> getAll();
   
}
