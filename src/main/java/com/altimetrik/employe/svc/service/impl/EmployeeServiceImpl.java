package com.altimetrik.employe.svc.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.altimetrik.employe.svc.dto.DepartmentDTO;
import com.altimetrik.employe.svc.dto.EmployeeDTO;
import com.altimetrik.employe.svc.entity.Employee;
import com.altimetrik.employe.svc.exception.DataAlreadyExistsException;
import com.altimetrik.employe.svc.exception.ResourceNotFoundException;
import com.altimetrik.employe.svc.repo.EmployeeRepo;
import com.altimetrik.employe.svc.service.ApiClient;
import com.altimetrik.employe.svc.service.EmployeeService;
import com.altimetrik.employe.svc.utills.ObjectMapper;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeRepo employeeRepo;
	private ApiClient apiClient;
	
	@Override
	public EmployeeDTO doSave(EmployeeDTO dto) {
		
		Optional<Employee> dbData = employeeRepo.findByEmail(dto.getEmail());
		if(dbData.isPresent()) {
			throw new DataAlreadyExistsException("Email already exists.");
		}
		
		Employee emp = ObjectMapper.MAPPER.mapToDepartment(dto);
		Employee saveEMP = employeeRepo.save(emp);
		return ObjectMapper.MAPPER.mapToDepartmentDTO(saveEMP);
	}


	@Override
	public List<EmployeeDTO> getAll() {

		List<Employee> list = employeeRepo.findAll();
		if (null != list) {
			return list.stream().map(emp -> ObjectMapper.MAPPER.mapToDepartmentDTO(emp))
					.collect(Collectors.toList());
		}
		return new ArrayList<>();

	}


	@Override
	public EmployeeDTO getById(Long id) {
		
		Employee emp  = employeeRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Employee", "id", id));
		DepartmentDTO departmentDTO = apiClient.getByCode(emp.getDepartmentCode());
		log.info("Found department:{} status for DepartmentCode:{}", departmentDTO != null ? "FOUND":"NOT_FOUND", emp.getDepartmentCode());
		
	    EmployeeDTO dto = 	ObjectMapper.MAPPER.mapToDepartmentDTO(emp);
	    if(null != departmentDTO && null != dto) {
	    	dto.setDepartmentName(departmentDTO.getName());
		}
		return   dto;
	}

}
