package com.altimetrik.employe.svc.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.altimetrik.employe.svc.dto.DepartmentDTO;

@FeignClient(url = "http://department-app-mydatabase.192.168.99.100.nip.io", name = "DEP-SVC")
public interface ApiClient {
	@GetMapping("api/depsvc/{code}")
	DepartmentDTO getByCode(@PathVariable String code);
}
