package com.altimetrik.employe.svc.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.altimetrik.employe.svc.entity.Employee;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {
   Optional<Employee> findByEmail(String email);
}
